'''
Created on Jul 28, 2023

@author: ubuntu
'''

import numpy as np
import numpy.random as rn


def make_y_greater_than_mx_plus_q(m, q, num=10, low=-10.0, high=10.0):
    '''Returns (randomly):
    - a list of num pairs (x,y)
    - a list of num labels: 
        - 0 if y <= m*x + q
        - 1 if y > m*x + q
    
    with low <= x <= high
    '''
    rn.seed()
    inputs = []
    labels = []
    for _ in range(num):
        x = rn.uniform(low, high)
        y = rn.uniform(low, high)
        inputs.append([x, y])
        labels.append(1 if y > m * x + q else 0)
    inputs = np.array(inputs)
    labels = np.array(labels).reshape(num, 1)
    return inputs, labels


def make_random_binary_row_sequence(num_row, num_col=1, seed=0):
    rn.seed(seed)
    num_elements = num_row * num_col
    sequence = rn.randint(0, 2, size=num_elements).reshape(num_row, num_col)
    return sequence


if __name__ == '__main__':
    pass
