'''
Created on Jul 15, 2023

@author: beopenbefree
'''

import numpy as np
import numpy.random as rn
import json
import weakref


# # Activation
class ActivationIdentity:

    def __call__(self, x):
        return x

    # derivative wrt input
    def derivative_dinput(self, _x):
        return 1


class ActivationReLU:

    def __call__(self, x):
        return np.maximum(0, x)

    # derivative wrt input
    def derivative_dinput(self, x):
        return (x > 0) * 1


class ActivationSigmoid:

    def __call__(self, x):
        return 1 / (1 + np.exp(-x))

    # derivative wrt input
    def derivative_dinput(self, x):
        sigma = self.__call__(x)
        return sigma * (1 - sigma)


# # Filter
class FilterActivationSoftmax:
    
    def forward(self, inputs):
        self.inputs = inputs
        exp_values = np.exp(inputs - np.max(inputs, axis=1, keepdims=True))
        self.output = exp_values / np.sum(exp_values, axis=1, keepdims=True)
        return self.output
        
    def backward(self, outputs, true_outputs, loss, general_case=False):
        partial_delta = None
        samples = len(outputs)
        # special case: loss == LossCategoricalCrossEntropy, true_outputs == one-hot encoded
        if (isinstance(loss, LossCategoricalCrossEntropy)) and (not general_case):
            # categorical sparse label list (column vector) 
            if true_outputs.shape[1] == 1:
                true_outputs = true_outputs[:, 0]
            # categorical one-hot encoded labels
            elif true_outputs.shape[1] > 1:
                true_outputs = np.argmax(true_outputs, axis=1)
            partial_delta = outputs.copy()
            partial_delta[range(samples), true_outputs] -= 1
            partial_delta /= samples
        # general case: generic loss
        else:
            partial_delta = np.empty(outputs.shape, dtype=float)
            partial_delta_loss = loss.derivative_doutput(outputs, true_outputs)
            for i, sample in enumerate(outputs):
                sample = sample.reshape(-1, 1)
                jacobian = np.diagflat(sample) - np.dot(sample, sample.T)
                partial_delta_sample = np.dot(partial_delta_loss[i], jacobian)
                partial_delta[i] = partial_delta_sample
        return partial_delta


# # Loss
class LossSquaredSum:
    
    def __call__(self, y, true_y):
        return np.sum(np.power(true_y - y, 2), axis=1, keepdims=True) * 0.5

    # derivative wrt output
    def derivative_doutput(self, y, true_y):
        # normalized derivative/gradient
        return -(true_y - y) / len(y)
    

class LossCategoricalCrossEntropy:

    def __call__(self, y, true_y):
        samples = len(y)
        y_clipped = np.clip(y, 1e-7, 1 - 1e-7)
        # categorical sparse label list (column vector) 
        if true_y.shape[1] == 1:
            correct_confidences = y_clipped[range(samples), true_y[:, 0]].reshape(-1, 1)
        # categorical one-hot encoded labels
        elif true_y.shape[1] > 1:
            correct_confidences = y_clipped[range(samples), np.argmax(true_y, axis=1)].reshape(-1, 1)
        return -np.log(correct_confidences)

    # derivative wrt output
    def derivative_doutput(self, y, true_y):
        # convert (if needed) categorical sparse label list
        # (column vector) in categorical one-hot encoded labels
        if true_y.shape[1] == 1:
            true_y = np.eye(len(y[0]))[true_y[:, 0]]
        # normalized derivative/gradient
        return (-true_y / y) / len(y)


class Neuron:
    
    def __init__(self, network, n_inputs, activation):
        self.network = weakref.ref(network)
        rn.seed(self.network().weight_seed)
        self.weights = self.network().weight_scale * rn.randn(n_inputs).astype("float")
        self.bias = np.zeros(1)
        self.activation = activation()
        self.inputs = np.zeros(n_inputs)
        self.sum = 0.0
    
    def forward(self, inputs):
        self.inputs = inputs
        self.sum = np.dot(self.inputs, self.weights) + self.bias
        self.output = self.activation(self.sum).reshape(-1, 1)
        return self.output
    
    def _f1(self):
        # derivative of activation wrt its input (i.e. the sum)
        return self.activation.derivative_dinput(self.sum)
    
    def backward(self, partial_delta):
        # multiplication with the activation derivative (wrt its input)
        self.delta = partial_delta * self._f1()
        # batch compute dw for all weights and db for bias
        # NOTE: multiplying by self.num_samples_inv we get the batch's mean
        db = self.network().eta * np.sum(self.delta)
        dw = self.network().eta * np.dot(self.delta, self.inputs) 
        if self.network().momentum:
            if not hasattr(self, "dw_momentums"):
                self.db_momentums = np.zeros_like(self.bias)
                self.dw_momentums = np.zeros_like(self.weights)
            db = self.network().momentum * self.db_momentums + db
            self.db_momentums = db
            dw = self.network().momentum * self.dw_momentums + dw
            self.dw_momentums = dw  
        self.bias += db
        self.weights += dw

    def __str__(self):
        out = "\t\tNeuron:\n"
        out += "\t\t\tWeights:"
        for weight in self.weights:
            out += str(weight) + ", "
        out += "- Bias : " + str(self.bias) 
        return out
    
    def _dump(self):
        return  {
                    "weights": [w for w in self.weights],
                    "bias": self.bias,
                 }

    def load(self, json_dict):
        for w in range(len(json_dict["weights"])):
            self.weights[w] = json_dict["weights"][w]
        self.bias = json_dict["bias"]


class LayerDense:

    def __init__(self, network, n_inputs, n_neurons, activation):
        self.network = weakref.ref(network)
        self.neurons = np.array([Neuron(self.network(), n_inputs, activation) for _ in range(n_neurons)])        
        # for this layer we create a weight_table: element wij is the weight
        # between neuron i of this layer and neuron j of preceding layer
        self.weight_table = np.empty((n_neurons, n_inputs)).astype("float")
    
    def forward(self, inputs):
        # for this layer we create a delta table (if needed)
        if (not hasattr(self, "delta")): #or (len(self.delta) != len(inputs)):
            self.delta = np.empty((len(inputs), len(self.neurons))).astype("float")
        res = []
        for i, neuron in enumerate(self.neurons):
            res.append(neuron.forward(inputs))
            self.weight_table[i,:] = neuron.weights
        return np.concatenate(res, axis=1)
    
    def backward(self, partial_delta):
        for i, neuron in enumerate(self.neurons):
            neuron.backward(partial_delta[:, i])
            self.delta[:, i] = neuron.delta
    
    def __str__(self):
        out = "\tLayer:\n"
        for neuron in self.neurons:
            out += neuron.__str__() + "\n" 
        return out

    def _dump(self):
        return [n._dump() for n in self.neurons]

    def load(self, json_list):
        for n in range(len(json_list)):
            self.neurons[n].load(json_list[n])
    

class NetworkDeep:

    def __init__(self, n_inputs, layer_descripitions, output_filter=None,
                 eta=0.01, weight_seed=None, weight_scale=0.01,
                 eta_decay=None, momentum=None):
        self.eta = eta
        self.starting_eta = eta
        self.eta_decay = eta_decay  # decay rate
        self.momentum = momentum  # momentum
        self.iterations = 0
        self.output_filter = output_filter() if output_filter else None
        self.layers = []
        self.weight_seed = weight_seed
        self.weight_scale = weight_scale
        curr_n_inputs = None
        for layer_descr in layer_descripitions:
            n_neurons, activation = layer_descr
            if len(self.layers) == 0:  # input layer
                self.layers.append(LayerDense(self, n_inputs, n_neurons, activation))
            else:
                self.layers.append(LayerDense(self, curr_n_inputs, n_neurons, activation))
            curr_n_inputs = n_neurons
    
    def update_params(self):
        self.iterations += 1
        if self.eta_decay:
            self.eta = (self.starting_eta * 
                        (1.0 / (1.0 + self.eta_decay * self.iterations)))
        
    def reset_params(self):
        self.eta = self.starting_eta
        self.iterations = 0

    def forward(self, inputs):
        curr_outputs = None
        for i in range(len(self.layers)):
            if i == 0:  # input layer
                curr_outputs = self.layers[i].forward(inputs)
            else:
                curr_outputs = self.layers[i].forward(curr_outputs)
        if self.output_filter:
            curr_outputs = self.output_filter.forward(curr_outputs)
        return curr_outputs

    def backward(self, outs, true_outs, loss, **filter_kwargs):
        partial_delta = None
        # 
        for i in reversed(range(len(self.layers))):
            # NOTE: partial_delta(s) are the delta(s) of the current layer without the
            # multiplication of the activation derivative wrt its input, that is
            # derivative_dinput(): the backward pass will perform this multiplication
            # for each neuron, so as to obtain the correct delta(s) for this layer.  
            if i == len(self.layers) - 1:
                # output layer
                if self.output_filter:
                    partial_delta = -self.output_filter.backward(outs, true_outs, loss, **filter_kwargs)
                else:
                    partial_delta = -loss.derivative_doutput(outs, true_outs)
            else:
                # inner layer
                partial_delta = np.dot(self.layers[i + 1].delta,
                                           self.layers[i + 1].weight_table)
            self.layers[i].backward(partial_delta)
    
    def __str__(self):
        out = "Neural Network:\n"
        for layer in self.layers:
            out += layer.__str__() + "\n"
        return out
    
    def _dump(self):
        return [l._dump() for l in self.layers],
    
    def save(self, json_file):
        f = open(json_file, "w")
        json.dump(self._dump()[0], f)
        f.close()
    
    def load(self, json_file):
        f = open(json_file, "r")
        json_load = json.load(f)
        for l in range(len(json_load)):
            self.layers[l].load(json_load[l])
        f.close()


def accuracy_classifier(outputs, class_targets):
    predictions = np.argmax(outputs, axis=1).reshape(-1, 1)
    # check if categorical one-hot encoded labels
    if class_targets.shape[1] != 1:
        class_targets = np.argmax(class_targets, axis=1).reshape(-1, 1)
    return np.mean(predictions == class_targets)
