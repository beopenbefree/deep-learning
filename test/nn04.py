'''
Created on Aug 13, 2023

@author: ubuntu
'''

import numpy as np
import time

from src.mynn import NetworkDeep, ActivationReLU, ActivationIdentity, ActivationSigmoid, \
    LossCategoricalCrossEntropy, FilterActivationSoftmax, accuracy_classifier

from tkinter import *
import matplotlib.pyplot as plt
import nnfs
from nnfs.datasets import spiral_data
nnfs.init()

# Data: training
# input size = batch_size * batch_num
batch_size = 100
batch_num = 1
epoch_num = 10001

# Data: neural network
eta = 1.0  # the learning rate
eta_decay = 0.001  # decay rate
momentum = 0.9  # momentum
filter_kwargs = {"general_case": False}
loss = LossCategoricalCrossEntropy()
output_filter = FilterActivationSoftmax
weight_seed = None
weight_scale = 0.01
n_inputs = 2
layer_descripitions = [
    (64, ActivationReLU),
    # (64, ActivationSigmoid,),
    (3, ActivationIdentity)]

if __name__ == '__main__':
    # create neural network
    net = NetworkDeep(n_inputs=n_inputs, layer_descripitions=layer_descripitions,
                      output_filter=output_filter, eta=eta,
                      weight_seed=weight_seed, weight_scale=weight_scale,
                      eta_decay=eta_decay, momentum=momentum)
    # training data
    batch_inputs, batch_true_outputs = spiral_data(
            samples=batch_size, classes=3)
    batch_true_outputs = batch_true_outputs.reshape(-1, 1)
    # plot data
    plt.ion()
    start_fig, start_ax = plt.subplots(figsize=(10, 8))
    start_ax.scatter(batch_inputs[:, 0], batch_inputs[:, 1],
                c=batch_true_outputs, cmap='brg')
    start_fig.canvas.draw()
    start_fig.canvas.flush_events()
    
    end_fig, end_ax = plt.subplots(figsize=(10, 8))
    
    # plt.show()
    
    # training
    start_time = time.time()
    for epoch in range(epoch_num):
        for batch in range(batch_num):
            # batch_inputs, batch_true_outputs = spiral_data(
            #         samples=batch_size, classes=3)
            # batch_true_outputs = batch_true_outputs.reshape(-1, 1)
            # forward pass
            batch_outputs = net.forward(batch_inputs)
            # loss computation
            batch_losses = loss(batch_outputs, batch_true_outputs)
            # backward pass
            net.backward(batch_outputs, batch_true_outputs, loss, **filter_kwargs)
            # update params
            net.update_params()
        #
        if (epoch != 0) and (epoch % 1000) == 0:
            # print("Epoch: ", epoch, "\n", net)
            print("Epoch: ", epoch)
            print("Loss (mean): ", np.mean(batch_losses))
            print("Accuracy: ", accuracy_classifier(batch_outputs, batch_true_outputs))
            print("learning rate: ", net.eta)
            print("\n")
            # plot data
            true_outputs = np.argmax(batch_outputs, axis=1)
            # end_fig, end_ax = plt.subplots(figsize=(10, 8))
            end_ax.scatter(batch_inputs[:, 0], batch_inputs[:, 1],
                        c=true_outputs, cmap='brg')
            end_fig.canvas.draw()
            end_fig.canvas.flush_events()
            # plt.show()
    plt.ioff()
    
    end_time = time.time()
    # print("Net:\n", net)
    print("\nElapsed time: ", end_time - start_time)
