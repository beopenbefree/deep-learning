'''
Created on Aug 23, 2023

@author: ubuntu
'''

import numpy as np
import time

from src.nnfs_lib import Layer_Dense, Activation_ReLU, Activation_Softmax_Loss_CategoricalCrossentropy, \
    Optimizer_SGD

import nnfs
from nnfs.datasets import spiral_data

nnfs.init()

if __name__ == '__main__':
    
    # Create dataset
    X, y = spiral_data(samples=100, classes=3)
    
    # Create Dense layer with 2 input features and 64 output values
    dense1 = Layer_Dense(2, 64)
    
    # Create ReLU activation (to be used with Dense layer):
    activation1 = Activation_ReLU()
    
    # Create second Dense layer with 64 input features (as we take output
    # of previous layer here) and 3 output values (output values)
    dense2 = Layer_Dense(64, 3)
    
    # Create Softmax classifier's combined loss and activation
    loss_activation = Activation_Softmax_Loss_CategoricalCrossentropy()
    
    # Create optimizer
    optimizer = Optimizer_SGD(decay=1e-3, momentum=0.9)
    # Train in loop
    start_time = time.time()
    for epoch in range(10001):
        # Perform a forward pass of our training data through this layer
        dense1.forward(X)
        
        # Perform a forward pass through activation function
        # takes the output of first dense layer here
        activation1.forward(dense1.output)
        
        # Perform a forward pass through second Dense layer
        # takes outputs of activation function of first layer as inputs
        dense2.forward(activation1.output)
        
        # Perform a forward pass through the activation/loss function
        # takes the output of second dense layer here and returns loss
        loss = loss_activation.forward(dense2.output, y)
        
        # Calculate accuracy from output of activation2 and targets
        # calculate values along first axis
        predictions = np.argmax(loss_activation.output, axis=1)
        accuracy = np.mean(predictions == y)
        if not epoch % 100:
            print(f'epoch: {epoch}, ' + 
                f'acc: {accuracy:.3f}, ' + 
                f'loss: {loss:.3f}, ' + 
                f'lr: {optimizer.current_learning_rate}')
        
        # Backward pass
        loss_activation.backward(loss_activation.output, y)
        dense2.backward(loss_activation.dinputs)
        activation1.backward(dense2.dinputs)
        dense1.backward(activation1.dinputs)
        
        # Update weights and biases
        optimizer.pre_update_params()
        optimizer.update_params(dense1)
        optimizer.update_params(dense2)
        optimizer.post_update_params()
    
    end_time = time.time()
    # print("Net:\n", net)
    print("\nElapsed time: ", end_time - start_time)
