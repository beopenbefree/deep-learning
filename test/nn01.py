'''
Created on Aug 3, 2023

@author: ubuntu
'''

import numpy as np
import numpy.random as rn
import time
from src.mynn import NetworkDeep, ActivationSigmoid, LossSquaredSum
from src.generate_data import make_random_binary_row_sequence

# # batch learning: XOR

# Data: training
# input size = batch_size * batch_num
batch_size = 1
batch_num = 20000
epoch_num = 1

# Data: neural network
n_inputs = 2
eta = 0.25
filter_kwargs = {"general_case": False}
loss = LossSquaredSum()
output_filter = None
layer_descripitions = [(2, ActivationSigmoid),
                       (1, ActivationSigmoid)]

if __name__ == '__main__':
    # create neural network
    
    net = NetworkDeep(n_inputs=2, layer_descripitions=layer_descripitions,
                      eta=eta, weight_seed=None, weight_scale=0.01)
    start_time = time.time()   
    for epoch in range(epoch_num):
        for batch in range(batch_num):
            # training data
            batch_inputs = make_random_binary_row_sequence(batch_size, 2, seed=batch)
            rn.shuffle(batch_inputs)
            batch_true_outputs = (batch_inputs[:, 0] ^ batch_inputs[:, 1]).reshape(-1, 1)
            # forward pass
            batch_outputs = net.forward(batch_inputs)
            # loss computation
            batch_losses = loss(batch_outputs, batch_true_outputs)
            # backward pass
            net.backward(batch_outputs, batch_true_outputs, loss)
        #
        print("Epoch: ", epoch, "\n", net)
        print("Loss (mean): ", np.mean(batch_losses))
    #
    test_batch_inputs = np.array([[0, 0],
                                  [1, 0],
                                  [0, 1],
                                  [1, 1], ])
    test_batch_outputs = net.forward(test_batch_inputs)
    print(test_batch_outputs)
    #
    end_time = time.time()
    print("\nElapsed time: ", end_time - start_time)
