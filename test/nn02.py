'''
Created on Aug 3, 2023

@author: ubuntu
'''

import numpy as np
from src.mynn import NetworkDeep, ActivationSigmoid, LossSquaredSum


def batch_learning(net, batch_inputs, batch_true_outputs):
    # batch learning XOR
    # forward pass
    batch_outputs = net.forward(batch_inputs)
    # loss computation
    batch_losses = loss(batch_outputs, batch_true_outputs)
    # print(batch_losses)
    # backward pass
    net.backward(batch_outputs, batch_true_outputs, loss)
    #
    print(net)
    print(batch_outputs)
    print(batch_losses)


def set_weights_biases(net, w, b):
    # set weights, biases to known values
    for l in net.layers:
        for n in l.neurons:
            s = len(n.weights)
            n.weights = np.ones((s,)) * w
            n.bias = b    


if __name__ == '__main__':
    layer_descripitions = [(2, ActivationSigmoid),
                           (1, ActivationSigmoid)]
    eta = 0.25
    net = NetworkDeep(n_inputs=2, layer_descripitions=layer_descripitions, eta=eta)
    loss = LossSquaredSum()
    # 1
    w = 2.0
    b = 1.0
    set_weights_biases(net, w, b)
    batch_inputs = np.array([[0, 1]])
    batch_true_outputs = np.array([[1]])
    batch_learning(net, batch_inputs, batch_true_outputs)
    # 2
    w = 2.0
    b = 1.0
    set_weights_biases(net, w, b)
    batch_inputs = np.array([[1, 1]])
    batch_true_outputs = np.array([[0]])
    batch_learning(net, batch_inputs, batch_true_outputs)
    # 1, 2
    w = 2.0
    b = 1.0
    set_weights_biases(net, w, b)
    batch_inputs = np.array([[0, 1],
                             [1, 1]])
    batch_true_outputs = np.array([[1],
                                   [0]])
    batch_learning(net, batch_inputs, batch_true_outputs)
    #
    print(net._dump())
    net.save("net.json")
    #
    net1 = NetworkDeep(n_inputs=2, layer_descripitions=layer_descripitions, eta=eta)
    net1.load("net.json")
    print(net1._dump())

