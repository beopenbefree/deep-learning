'''
Created on Aug 3, 2023

@author: ubuntu
'''

import numpy.random as rn
from src.mynn import NetworkDeep, ActivationReLU, FilterActivationSoftmax, \
    LossCategoricalCrossEntropy, LossSquaredSum
from src.generate_data import make_random_binary_row_sequence

if __name__ == '__main__':
    layer_descripitions = [(2, ActivationReLU),
                           (2, ActivationReLU)]
    # net = NetworkDeep(n_inputs=2, layer_descripitions=layer_descripitions,
    #                   output_filter=FilterActivationSoftmax, eta=0.01)
    net = NetworkDeep(n_inputs=2, layer_descripitions=layer_descripitions,
                      output_filter=None, eta=0.01)
    loss = LossSquaredSum()
    # batch learning
    batch_size = 3
    batch_inputs = make_random_binary_row_sequence(batch_size, 2, seed=0)
    rn.shuffle(batch_inputs)
    batch_true_outputs = (batch_inputs[:, 0] ^ batch_inputs[:, 1]).reshape(-1, 1)
    # forward pass
    batch_outputs = net.forward(batch_inputs)
    # loss computation
    batch_losses = loss(batch_outputs, batch_true_outputs)
    # backward pass
    net.backward(batch_outputs, batch_true_outputs, loss)
    #
    print(batch_losses)
    print(net)
