'''
Created on Aug 13, 2023

@author: ubuntu
'''

# Test on Softmax and Categorical cross entropy loss

import numpy as np
from timeit import timeit

from src.mynn import LossCategoricalCrossEntropy, FilterActivationSoftmax

if __name__ == '__main__':
    so = np.array([[0.7, 0.1, 0.2],
               [0.1, 0.5, 0.4],
               [0.02, 0.9, 0.08]])
    sl = np.array([[0],
                   [1],
                   [1]])
    oe = np.array([[1, 0, 0],
               [0, 1, 0],
               [0, 1, 0]])
    
    loss = LossCategoricalCrossEntropy()
    sm = FilterActivationSoftmax()
    
    timeit_count = 100000
    
    print("\nspecific case (sl): \n", sm.backward(so, sl, loss))
    print("\ngeneral case (sl): \n", sm.backward(so, sl, loss, general_case=True))
    t_sl_specific = timeit(lambda: sm.backward(so, sl, loss), number=timeit_count)
    t_sl_general = timeit(lambda: sm.backward(so, sl, loss, general_case=True), number=timeit_count)
    print("t_sl_general/t_sl_specific :", t_sl_general / t_sl_specific)
    #
    print("\nspecific case (oe): \n", sm.backward(so, oe, loss))
    print("\ngeneral case (oe): \n", sm.backward(so, oe, loss, general_case=True))
    t_oe_specific = timeit(lambda: sm.backward(so, oe, loss), number=timeit_count)
    t_oe_general = timeit(lambda: sm.backward(so, oe, loss, general_case=True), number=timeit_count)
    print("t_oe_general/t_oe_specific :", t_oe_general / t_oe_specific)
    #
    print("\nt_sl_specific/t_oe_specific :", t_sl_specific / t_oe_specific)
    print("\nt_sl_general/t_oe_general :", t_sl_general / t_oe_general)
 