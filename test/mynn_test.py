'''
Created on Aug 10, 2023

@author: ubuntu
'''

from src.mynn import *

if __name__ == '__main__':
    outputs = np.array([[0.7, 0.2, 0.1],
                           [0.5, 0.1, 0.4],
                           [0.02, 0.9, 0.08]])
    class_targets_hot = np.eye(3)[np.array([0, 1, 1])]
    class_targets_cat = np.array([0, 1, 1]).reshape(-1, 1)
    print(accuracy_classifier(outputs, class_targets_hot))
    print(accuracy_classifier(outputs, class_targets_cat))
